# Text segments ontology


## Scope
Ontologie for generation, respectively redaction, of metadata about text segments for which are automatic indexes are generated, respectively manual indexes are redacted. Examples of metadata: language, script, period, type (volume, chapter, page, paragraph, footnote, endnote, etc.).
